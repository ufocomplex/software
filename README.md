# UFO software

Here is all UFO software repository!
It is contained in 3 branches
1. [android](https://gitlab.com/ufocomplex/software/tree/android) - here the Android app is being developed
2. [windows](https://gitlab.com/ufocomplex/software/tree/windows) - here the Windows app is being developed
3. [arduino](https://gitlab.com/ufocomplex/software/tree/arduino) - here the Arduino sketch is being developed

## Stable software versions
You can find stable version in [***Releases***](https://gitlab.com/ufocomplex/software/releases) tab.
## Terminology

| Abbreviation | Meaning |
| :-------: | :----: |
| RC | result code |
| TC | Task code |
| SAHMAC | Service Authentication Hmac |
| DN | Device Nonce |
| SSN | Session Nonce |
| SEN | Service Nonce |
| DH | Device Hmac |
| SH | Session Hmac |
| DK | Device key |
| AK | Authentication key |
| Host | Device, working with Windows Hello(laptop, PC...) |
| Target | Android smartphone |

Below the protocols will be described.
## Windows <--> Arduino protocol

COM-port emulation over USB is used.
Frequency - 115200 baud.

| Task | From Windows | From Arduino | 
|:--:|:------------------:|:------------------:| 
|Get target's id and name|TC|RC (1 byte), ID(36 bytes) , Target name (32 bytes)|
|Registration|TC, User Name(31 byte) , ";" , Host name (32 bytes)|RC (1 byte)|
|Authentication|TC, SAHMAC (32 bytes)  , DN (32 bytes)  , SSN (32 bytes)  , SEN (32 bytes)|RC (1 byte) , DH (32 bytes) , SH (32 bytes)|

## Arduino <--> Card protocol
### ***Attention! Using this method is not fully secure.***
Only ntag213, ntag215, ntag216 are supported.
Stability of other card types is not guaranteed.
As it is desribed in standard for these card types, you can write to a card from page 4.
That's why DK is written from page 4 till 11, and AK is written from page 12 till 19.
## Arduino <--> Android protocol
The host card emulation technology is used.
Before starting communication sending [SELECT APDU](https://developer.android.com/reference/android/nfc/cardemulation/HostApduService) is needed.

| Task | From Arduino | From Android | 
|:---:|:---:|:---:| 
|Get target's id and name|TC|RC (1 byte), ID(36 bytes) , Target name (32 bytes)|
|Registration|TC, User name(31 bytes) , ";" , Host name (32 bytes) |RC (1 byte)|
|Authentication|TC, SAHMAC (32 bytes)  , DN (32 bytes)  , SSN (32 bytes)  , SEN (32 bytes) | RC (1 byte) , DH (32 bytes) , SH (32 bytes)|

## Result codes (RC)
 
These codes can be used in any described data transferring protocols.

| Code | Meaning |
|:---:|:---:|
|0x00|All is Ok!|
|0x01|Time limit exceeded.|
|0x02|Authentication is not ok. (no matching keys, ..)|
|0x03|Card type is wrong.|
|0x04|Card authentication error.|
|0x05|Card write error.|
|0x06|Card read error.|
|0x07|Device not found.|
|0x08|Unknown error.|

## Task codes (TC)

| Code | Meaning |
|:---:|:---:|
|0x00|Get ID and target's name|
|0x01|Target registration|
|0x02|Target authentication|

    

